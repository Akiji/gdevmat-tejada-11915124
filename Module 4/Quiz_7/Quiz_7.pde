void setup()
{
  size(1280,720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0,-1, 0);
  walkerarrayactive();
  
  

}
PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = - (mouseY - Window.windowHeight / 2);
  return new PVector(x, y);
}


void draw()
{
  background(80);
  
  
  PVector mouse = mousePos();
  
  mousem.setposition(mouse);
  mwalkerarray();
  dwalkerarray();
  update();
  
}

void walkerarrayactive() 
{
  for(int i = 0; i < 100; i++)
  {
    float x = random(-360, 360);
    float y = random(-360, 360);
    walkerarray[i] = new Walker(x, y, random(5, 25));

  }
}

void dwalkerarray() 
{
  for(int i = 0; i < 100; i++)
  {
    walkerarray[i].render();
  }
}

void mwalkerarray() 
 {
   for(int i = 0; i < 100; i++)
    {
      PVector direction = PVector.sub(mousem.position, walkerarray[i].position);
      walkerarray[i].acceleration = direction.normalize().mult(0.2);
    }
 }


 void update()
 {
   for(int i = 0; i < 100; i++)
    {
      walkerarray[i].update();
    }
 }

PVector movement = new PVector();
Walker[] walkerarray= new Walker[100];
Walker mousem = new Walker(0,0,1);
