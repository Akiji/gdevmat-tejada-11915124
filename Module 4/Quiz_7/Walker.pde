public class Walker
{
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  public float velocityLimit = 10;
  public float scale;
 
  
 Walker(PVector acceleration)
 {
   this.acceleration = acceleration;
 }
 Walker(float x, float y, float scale)
 {
   position = new PVector(x, y);
   this.scale = scale;
 }

 Walker(PVector position, float scale)
 {
   this.position = position;
   this.scale = scale;
 }
 
  public void update()
  {
    this.velocity.add(this.acceleration);
    this.velocity.limit(velocityLimit);
    this.position.add(this.velocity);
  }
  public void render()
  {
    circle(position.x, position.y, scale);
  }
 public void setposition(PVector position)
  {
    this.position = position;
  }
}
