class Walker
{
  float x;
  float y;
  
  void render()
  {
    noStroke();
    int alpha = int(random(50,100));
    fill(random(255), random(255),random(255), alpha);
    circle(x,y,30);
  }
  void randomWalker()
  {
    int rng = int(random(8));
    
    if (rng == 0)
    {
      y+=10;
    }
    else if (rng == 1)
    {
      y-=10;
    }
    else if (rng == 2)
    {
      x+=10;
    }
    else if (rng == 3)
    {
      x-=10;
    }
    else if (rng == 4)
    {
      x+=10;
      y+=10;
    }
    else if (rng == 5)
    {
      x-=10;
      y-=10;
    }
    else if (rng == 6)
    {
      x+=10;
      y-=10;
    }
    else if (rng == 7)
    {
      x-=10;
      y+=10;
    }
  }
  
  void randomWalkBiased()
  {
    int rng = int(random(100));
    if (rng <=70)
    {
      x+=10;
    }
    else if (rng <=80)
    {
      x-=10;
    }
    else if (rng <=90)
    {
      y+=10;
    }
    else if (rng <=100)
    {
      y-=10;
    }
  }
}
