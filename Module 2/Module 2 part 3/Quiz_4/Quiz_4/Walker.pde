class Walker
{
  public float x;
  public float y;
  public float tx = 0, ty = 10000;
  public float tz = 5000;
  
  
  void render()
  {
    noStroke();
    int alpha = int(map(noise(ty),0,1,0,255));
    fill(map(noise(tx),0,1,0,255), map(noise(ty),0,1,0,255),map(noise(tz),0,1,0,255), alpha);
    
    circle(x,y,map(noise(ty),0,1,5,150));
  }
  
  void perlinWalker()
  {
    x = map(noise(tx),0,1,-510,510);
    y = map(noise(ty),0,1,-360,360);
    
    tx +=0.01f;
    ty +=0.01f;
    tz +=0.1f;
  }
}
