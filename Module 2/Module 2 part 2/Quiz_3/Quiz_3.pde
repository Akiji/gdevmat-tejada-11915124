void setup()
{
  size(1020,720,P3D);
  camera(0,0,-(height/2.0)/tan(PI*30.0/180.0),0,0,0,0,-1,0);
  background(0);
  
}

int frame = 0;
void draw()
{
  
  if(frame != 300)
  {
    psplatter();
    println(frame);
    frame++;
  
}
  else
 {
  background(255);
  frame = 0;
 }
}
void psplatter()
{
  float gaussian = randomGaussian();
  float standardDeviation = 150;
  float mean = 0;
  float x = standardDeviation * gaussian + mean;
  float y = random(-360, 360);
  
  noStroke();
  int alpha = int(random(50,100));
  fill (random(255),random(255),random(255), alpha);
  
  float gaussian1 = randomGaussian();
  float standardDeviation1 = 20;
  float mean1= 10;
  circle(x,y,(standardDeviation1 * gaussian1 + mean1));
  
}
