void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
 
  walkerarrayactive();
}  

Walker[] walkerarray = new Walker[10];


void draw()
{
  background(80);
  
  for(Walker walkerr: walkerarray)
  {
    walkerr.update();
    walkerr.render();
    
     for(Walker walkerr2: walkerarray)
     {
       if(walkerr != walkerr2)
       {
     walkerr2.applyForce(walkerr.calculateAttraction(walkerr2));
     walkerr.applyForce(walkerr2.calculateAttraction(walkerr));
       }
     }
  }

     
 
  
 
}
void walkerarrayactive()
{
  for(int i = 0; i < walkerarray.length; i++)
  {
    walkerarray[i] = new Walker();
    walkerarray[i].position = new PVector(random(-720, 720), random(-360, 360));
    walkerarray[i].mass = random(1,10);
    walkerarray[i].scale = walkerarray[i].mass * 15; 
    walkerarray[i].setcolor(random(255), random(255), random(255));
    
  }
}
