void setup()
{
  size(1080,720,P3D);
  camera(0,0, Window.eyeZ,0,0,0,0,-1,0);
  fill(0);
}

Walker thewalker = new Walker(0,0,50);
Walker[] walkerarray = new Walker[100];
int frame = 0;

void draw()
{
  background(0);
  if(frame == 0)
  for (int i = 0; i < 100; i++)
  {
  float gaussian = randomGaussian();
  float standardDeviation = 150;
  float mean = 0;
  float x = standardDeviation * gaussian + mean;
  float y = random(-360, 360);
  walkerarray[i] = new Walker(x,y, random(1,49));
  walkerarray[i].setColor(random(255),random(255),random(255),random(255));
  
  }
  
  if(frame != 300)
  {
     
  
  
  
  for(int i = 0; i < 100; i++)
  {
    walkerarray[i].render();
  }

  for (int i = 0; i < 100; i++)
  {
    PVector direction = PVector.sub(thewalker.position,walkerarray[i].position);
    walkerarray[i].position.add(direction.normalize().mult(2));
  }
  
  thewalker.render();
  thewalker.setColor(255, 255, 255, 255);
    frame++;
  
}
  else
 {
  background(255);
  frame = 0;
 }
   
 
}
