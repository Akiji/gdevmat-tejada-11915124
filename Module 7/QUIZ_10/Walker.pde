public class Walker
{
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  public float velocityLimit = 10;
  public float scale;
  public float mass;
  public float r = 255;
  public float g = 255;
  public float b = 255; 
  
 Walker()
 {
   position = new PVector();
 }
 Walker(float x, float y, float scale)
 {
   position = new PVector(x, y);
   this.scale = scale;
 }

 Walker(PVector position, float scale)
 {
   this.position = position;
   this.scale = scale;
 }
 void render()
 {
   noStroke();
   fill(r,g,b);
   circle(position.x, position.y, scale);
 }
 public void applyForce(PVector force)
 {
   PVector f = PVector.div(force, this.mass);
   this.acceleration.add(f);
 }
  
  Walker(PVector acceleration)
  {
    this.acceleration = acceleration;
  }
 public void setPosition(PVector position)
 {
   this.position = position;
 }
 public void update()
  {
    this.velocity.add(this.acceleration);
    this.velocity.limit(velocityLimit);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
  }
  public void setcolor(float r, float g, float b)
  {
    this.r = r;
    this.g = g;
    this.b = b;
  }
}
