void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  walkerarrayactive();
}

Walker[] walkerarray = new Walker[8];
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
PVector gravity = new PVector(0, -0.4);
PVector wind = new PVector(0.15,0);



void draw()
{
  background(255);
  ocean.render();
  for (Walker walkerr: walkerarray)
  {
  
  
  walkerr.update();
  walkerr.render();
  
  
  PVector gravity = new PVector(0, -0.25 * walkerr.mass);
  walkerr.applyForce(gravity);
  walkerr.applyForce(wind);
  
  float c = 0.1f;
  float normal = 1;
  float frictionMagnitude = c * normal;
  PVector friction = walkerr.velocity.copy();
  walkerr.applyForce(friction.mult(-1).normalize().mult(frictionMagnitude));
  
  
  if (walkerr.position.y <= Window.bottom)
  {
    walkerr.position.y = Window.bottom;
    walkerr.velocity.y *= -1;
  }
  
  if (ocean.isCollidingWith(walkerr))
  {
    PVector dragForce = ocean.calculateDragForce(walkerr);
    walkerr.applyForce(dragForce);
  }
  }
}
  void walkerarrayactive()
{
  int posX = walkerarray.length;
  for(int i = 0; i < walkerarray.length; i++)
  {
    posX = (Window.windowHeight / walkerarray.length) * (i - 3);
    walkerarray[i] = new Walker();
    walkerarray[i].position = new PVector(-posX, 350);
    walkerarray[i].mass = 1 + i;
    walkerarray[i].scale = walkerarray[i].mass * 15;
    walkerarray[i].setcolor(random(255), random(255), random(255));
  }
}
