void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  walkerarrayactivate();
}

Walker[] walkerarray = new Walker[8]; 

void draw()
{
  
  stroke(0);
  strokeWeight(10);
  background(80);
  line(0, 500, 0, -500);
  
  
  for(Walker walkerr: walkerarray)
  {
    
    float normal = 1;
    float mew = 0.01f;
    if(walkerr.position.x > 0)
    {
      mew = 0.4f;
    }
    float frictionMagnitude = mew * normal; 
    PVector friction = walkerr.velocity.copy();
    PVector right = new PVector(0.2, 0);
    friction.mult(-1); 
    friction.normalize();
    friction.mult(frictionMagnitude); 
    
    
    walkerr.update();
    walkerr.render();
    walkerr.applyForce(right);
    walkerr.applyForce(friction);
    
    

    if((walkerr.position.x >= Window.right)) 
    {
      walkerr.position.x = Window.right;
    }
    

 }
}

void walkerarrayactivate()
{
  int posY = walkerarray.length;
  for(int i = 0; i < walkerarray.length; i++)
  {
    posY = (Window.windowHeight / walkerarray.length) * (i - 3);
    walkerarray[i] = new Walker();
    walkerarray[i].position = new PVector(-350, -posY);
    walkerarray[i].mass = 1 + i;
    walkerarray[i].scale = walkerarray[i].mass * 15;
    walkerarray[i].setcolor(random(255), random(255), random(255));
  }
}
