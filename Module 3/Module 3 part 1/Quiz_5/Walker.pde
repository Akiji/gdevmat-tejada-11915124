class Walker
{
  PVector position = new PVector();
  PVector speed = new PVector(5,8);
  
  void render()
  {
  noStroke();
  circle(position.x, position.y, 50);
}


 void Bounce()
 {
   position.add(speed);
   
   if((position.x > Window.right) || (position.x < Window.left))
   {
     speed.x *= -1;
     fill(random(255),random(255),random(255));
   }
   
   if((position.y > Window.top) || (position.y < Window.bottom))
   {
     speed.y *= -1;
     fill(random(255),random(255),random(255));
   }
 }
}
