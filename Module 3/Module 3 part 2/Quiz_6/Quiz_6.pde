void setup()
{
  size(1080,720,P3D);
  camera(0,0, Window.eyeZ,0,0,0,0,-1,0);
  fill(0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth/2;
  float y = -(mouseY - Window.windowHeight/2);
  return new PVector(x,y);
}

void draw()
{
  background(130);
  
  strokeWeight(10);
  stroke(255,0,0);
  
  PVector mouse = mousePos();
  
  mouse.normalize().mult(250);
  line(0,0,mouse.x,mouse.y);
  line(0,0,-mouse.x,-mouse.y);
  
  strokeWeight(4);
  stroke(255,255,255);
  mouse.normalize().mult(250);
  line(0,0,mouse.x,mouse.y);
  line(0,0,-mouse.x,-mouse.y);
  
  strokeWeight(10);
  stroke(0,0,0);
  mouse.normalize().mult(50);
  line(0,0,mouse.x,mouse.y);
  line(0,0,-mouse.x,-mouse.y);
  
}
