void setup()
{
  size(1280,720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0,-1, 0);
  walkerarrayactive();
  
}
PVector gravity = new PVector(0, -0.4);
PVector wind = new PVector(0.15,0);
Walker[] walkerarray = new Walker[10];


void draw()
{
  background(80);
  for (Walker walkerr: walkerarray)
  {
  walkerr.render();
  walkerr.update();
  walkerr.applyForce(wind);
  walkerr.applyForce(gravity);
  
  if((walkerr.position.x >= Window.right) || (walkerr.position.x <= Window.left))
   {
     
     walkerr.velocity.x *= -1;
    
   }
   
   if((walkerr.position.y >= Window.top) || (walkerr.position.y <= Window.bottom))
   {
     
     walkerr.velocity.y *= -1;
    
   }
  }
}  
void walkerarrayactive()
{
  for(int i = 0; i < walkerarray.length; i++)
  {
    float x = -500;
    float y = 200;
    float mass = random(1, 10);
    walkerarray[i] = new Walker(x,y,mass*15);
    walkerarray[i].mass = mass;
    walkerarray[i].setcolor(random(255), random(255), random(255));
    
  }
}
